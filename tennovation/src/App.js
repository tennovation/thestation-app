import React from 'react';
import './App.css';
import Routes from './Routes';
import NavBar from './NavBar/NavBar';

export default function App() {
  
  return (
    <div className="App">
      <NavBar />
      <Routes />
    </div>
  );
}


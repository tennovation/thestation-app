import React from "react";
import {Route} from "react-router-dom";
import HomePage from "./Home/HomePage";
import SignInPage from './SignIn/SignInPage';
import CreateStationPage from './CreateStation/CreateStationPage'
import AuthRoute from './AuthRoute';

export default function Routes(){
    return(
        <div className="routes">
            <Route exact path = "/signIn" component = {SignInPage} />
            <Route exact path= "/" component = {HomePage} />
            <AuthRoute exact path = "/create" component = {CreateStationPage} />
        </div>
    )
}
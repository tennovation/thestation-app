import React from 'react';
import firebase from "../firebase";
import Auth from "../Auth";


export default function SignInPage() {
  var provider = new firebase.auth.GoogleAuthProvider();


  function SignInWithGoogle () {
  firebase.auth().signInWithPopup(provider).then(function(result) {
    // This gives you a Google Access Token. You can use it to access the Google API.
    var token = result.credential.accessToken;
    // The signed-in user info.
    var user = result.user;
    console.log(user.displayName);
    console.log("signed in");
    Auth.saveUserName(user.displayName);

    // ...
  }).catch(function(error) {
    // Handle Errors here.
    
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;

    console.log("ERROR WHEN sgning in");
    // ...
  });
}

function SignInOutGoogle (){
  firebase.auth().signOut().then(function() {
    console.log("signed out");
    // Sign-out successful.
  }).catch(function(error) {
    // An error happened.
    console.log("ERROR WHEN signing out");
  });
  
}
  return (
    <div className="App">
      <button onClick = {SignInWithGoogle}>SIGN IN </button>

      <button onClick = {SignInOutGoogle}>SIGN OUT </button>
    
    </div>
  );
}


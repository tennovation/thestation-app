import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import './NavBar.scss';
import Logo from '../Images/TVLogo.jpeg';



export default function NavBar () {
    return (
        <div className = "NavBar">
            
            <NavLink className = "HomeLink" to="/" >
                <div className = "Logo" >
                <img className= "LogoImage" src= {Logo} />
                </div>
            </NavLink>
           

            
            <NavLink className = "SignInLink" to="/signIn" >
                <h1 className = "SignInText">Sign In </h1>
            
            </NavLink>
           

            <NavLink className = "CreateLink" to="/create" >
                    <h1 className = "CreateText">Create  </h1>
            </NavLink>
       

            
            <NavLink className = "HostLink" to="/host" >
                    <h1 className = "HostText">Host </h1>
            </NavLink>
        

            <NavLink className = "JoinLink" to="/join" >
                    <h1 className = "JoinText">Host </h1>
            </NavLink>
            

        </div>
    )
}
